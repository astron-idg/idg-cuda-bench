#include <complex>
#include <iomanip>
#include <iostream>

#include <cudawrappers/cu.hpp>
#include <cudawrappers/nvrtc.hpp>

#include "kernels/kernels.h"

#if defined(ENABLE_PMT)
#include <pmt.h>
#endif

// Number of times to run each kernel
#define NR_REPETITIONS 5

// Number of times to run each benchmark
#define NR_BENCHMARKS 5

void report(std::string name, double seconds, float gflops, float gbytes,
            unsigned long mvis) {
  int w1 = 20;
  int w2 = 7;
  std::cout << std::setw(w1) << std::string(name) << ": ";
  std::cout << std::setprecision(2) << std::fixed;
  std::cout << std::setw(w2) << seconds * 1e3 << " ms";
  if (gflops != 0) {
    std::cout << ", " << std::setw(w2) << gflops / seconds << " GFlops/s";
  }
  if (gbytes != 0) {
    std::cout << ", " << std::setw(w2) << gbytes / seconds << " GB/s";
  }
  if (gflops != 0 && gbytes != 0) {
    float arithmetic_intensity = gflops / gbytes;
    std::cout << ", " << std::setw(w2) << arithmetic_intensity << " Flop/byte";
  }
  if (mvis != 0) {
    std::cout << ", " << std::setw(w2) << mvis / seconds << " MVis/s";
  }
}

void report(std::string name, double seconds, float gflops, float gbytes,
            unsigned long mvis, double joules) {
  report(name, seconds, gflops, gbytes, mvis);
  int w2 = 7;
  if (joules != 0) {
    double watt = joules / seconds;
    double efficiency = gflops / joules;
    std::cout << ", " << std::setw(w2) << watt << " W";
    std::cout << ", " << std::setw(w2) << efficiency << " GFlops/W";
  }
}

void run_kernel(const char *name, cu::Stream &stream, cu::Function &function,
                dim3 gridDim, dim3 blockDim,
                std::vector<const void *> &parameters, float gflops,
                float gbytes, unsigned long mvis = 0) {
#if not defined(ENABLE_PMT)
  cu::Event start, end;
#else
  char *pmt_name_char = std::getenv("PMT_NAME");
  std::string pmt_name = pmt_name_char ? std::string(pmt_name_char) :
#if defined(__HIP_PLATFORM_AMD__)
                                       "rocm";
#else
                                       "nvidia";
#endif
  std::unique_ptr<pmt::PMT> sensor = pmt::Create(pmt_name);
  pmt::State start, end;
#endif

  // Warmup
#if defined(ENABLE_PMT)
  for (int i = 0; i < NR_REPETITIONS; i++) {
    stream.launchKernel(function, gridDim.x, gridDim.y, gridDim.z, blockDim.x,
                        blockDim.y, blockDim.z, 0, parameters);
  }
#endif

  for (int i = 0; i < NR_BENCHMARKS; i++) {
    // Start measurement
#if not defined(ENABLE_PMT)
    stream.record(start);
#else
    stream.synchronize();
    start = sensor->Read();
#endif

    // Launch kernel
    for (int i = 0; i < NR_REPETITIONS; i++) {
      stream.launchKernel(function, gridDim.x, gridDim.y, gridDim.z, blockDim.x,
                          blockDim.y, blockDim.z, 0, parameters);
    }

    // End measurement
#if not defined(ENABLE_PMT)
    stream.record(end);
#endif

    // Wait for execution to finish
    stream.synchronize();

    // End measurement
#if defined(ENABLE_PMT)
    end = sensor->Read();
#endif

    // Report
#if not defined(ENABLE_PMT)
    double seconds = end.elapsedTime(start) * 1e-3;
    seconds /= NR_REPETITIONS;
    report(name, seconds, gflops, gbytes, mvis);
#else
    double seconds = pmt::PMT::seconds(start, end);
    seconds /= NR_REPETITIONS;
    double joules = pmt::PMT::joules(start, end);
    joules /= NR_REPETITIONS;
    report(name, seconds, gflops, gbytes, mvis, joules);
#endif
    std::cout << std::endl;
  }
}

void run_peak_flops(cu::Device &device, cu::Stream &stream) {
  // Parameters
  int multiProcessorCount =
      device.getAttribute<CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT>();
  int maxThreadsPerBlock =
      device.getAttribute<CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK>();

  // Amount of work performed
  double gflops = (1e-9 * multiProcessorCount * maxThreadsPerBlock) *
                  (1ULL * 2 * 2 * 16384 * 1024);
  double gbytes = 0;

  // Compile kernel
  std::vector<std::string> options = {
#if defined(__HIP__)
    "--offload-arch=" + device.getArch(),
#else
    "-arch=" + device.getArch()
#endif
  };
  nvrtc::Program program(GetSourcePeakFlops(), "peak_flops.cu");
  try {
    program.compile(options);
  } catch (nvrtc::Error &error) {
    std::cerr << program.getLog();
    throw;
  }
  cu::Module module(static_cast<const void *>(program.getPTX().data()));
  cu::Function function(module, "kernel_fma");

  // Kernel dimensions
  dim3 gridDim(multiProcessorCount);
  dim3 blockDim(maxThreadsPerBlock);

  // Setup parameters
  cu::DeviceMemory d_data(multiProcessorCount * maxThreadsPerBlock *
                          sizeof(float));
  std::vector<const void *> parameters = {d_data.parameter()};

  // Run kernel
  run_kernel("peak_flops", stream, function, gridDim, blockDim, parameters,
             gflops, gbytes);
}

/*
 * IDG types
 */
typedef struct {
  int x, y, z;
} Coordinate;

typedef struct {
  unsigned int station1, station2;
} Baseline;

typedef struct {
  int baseline_offset;
  int time_offset;
  int nr_timesteps;
  int aterm_index;
  Baseline baseline;
  Coordinate coordinate;
} Metadata;

/*
    Operation and byte count
*/
uint64_t flops_gridder(uint64_t nr_channels, uint64_t nr_timesteps,
                       uint64_t nr_subgrids, uint64_t subgrid_size,
                       uint64_t nr_correlations) {
  // Number of flops per visibility
  uint64_t flops_per_visibility = 0;
  flops_per_visibility += 5;                                 // phase index
  flops_per_visibility += 5;                                 // phase offset
  flops_per_visibility += nr_channels * 2;                   // phase
  flops_per_visibility += nr_channels * nr_correlations * 8; // update

  // Number of flops per subgrid
  uint64_t flops_per_subgrid = 0;
  flops_per_subgrid += 6; // shift

  // Total number of flops
  uint64_t flops_total = 0;
  flops_total +=
      nr_timesteps * subgrid_size * subgrid_size * flops_per_visibility;
  flops_total += nr_subgrids * subgrid_size * subgrid_size * flops_per_subgrid;
  return flops_total;
}

uint64_t bytes_gridder(uint64_t nr_channels, uint64_t nr_timesteps,
                       uint64_t nr_subgrids, uint64_t subgrid_size,
                       uint64_t nr_correlations) {
  // Number of bytes per uvw coordinate
  uint64_t bytes_per_uvw = 0;
  bytes_per_uvw += 1ULL * 3 * sizeof(float); // read uvw

  // Number of bytes per visibility
  uint64_t bytes_per_vis = 0;
  bytes_per_vis += 1ULL * nr_channels * nr_correlations * 2 *
                   sizeof(float); // read visibilities

  // Number of bytes per pixel
  uint64_t bytes_per_pix = 0;
  bytes_per_pix += 1ULL * nr_correlations * 2 * sizeof(float); // read pixel
  bytes_per_pix += 1ULL * nr_correlations * 2 * sizeof(float); // write pixel

  // Number of bytes per aterm
  uint64_t bytes_per_aterm = 0;
  bytes_per_aterm +=
      1ULL * 2 * nr_correlations * 2 * sizeof(float); // read aterm

  // Number of bytes per spheroidal
  uint64_t bytes_per_spheroidal = 0;
  bytes_per_spheroidal += 1ULL * sizeof(float); // read spheroidal

  // Total number of bytes
  uint64_t bytes_total = 0;
  bytes_total += 1ULL * nr_timesteps * bytes_per_uvw;
  bytes_total += 1ULL * nr_timesteps * bytes_per_vis;
  bytes_total +=
      1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_pix;
  bytes_total +=
      1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_aterm;
  bytes_total +=
      1ULL * nr_subgrids * subgrid_size * subgrid_size * bytes_per_spheroidal;
  return bytes_total;
}

void run_idg_gridder(cu::Device &device, cu::Stream &stream) {
  // Compile kernel
  std::vector<std::string> options = {
#if defined(__HIP__)
    "--offload-arch=" + device.getArch(),
    "-ffast-math",
#else
    "-arch=" + device.getArch(),
#endif
    "-I" + nvrtc::findIncludePath()
  };
  nvrtc::Program program(GetSourceGridder(), "gridder");
  try {
    program.compile(options);
  } catch (nvrtc::Error &error) {
    std::cerr << program.getLog();
    throw;
  }
  cu::Module module(static_cast<const void *>(program.getPTX().data()));
  cu::Function function(module, "kernel_gridder");

  // Setup parameters
  const int grid_size = 8192;
  const unsigned nr_correlations = 4;
  const int subgrid_size = 32;
  const float image_size = 0.01;
  const float w_step = 0;
  const int nr_channels = 16;
  const int nr_stations = 30;
  const int nr_timeslots = 64;
  const int nr_timesteps = 128; // per subgrid

  // Derived parameters
  auto nr_baselines = (nr_stations * (nr_stations - 1)) / 2;
  auto nr_subgrids = nr_baselines * nr_timeslots;
  auto total_nr_timesteps = nr_subgrids * nr_timesteps;
  auto mvis = 1e-6 * total_nr_timesteps * nr_channels;

  // Amount of work performed
  auto gflops =
      1e-9 * flops_gridder(nr_channels, total_nr_timesteps, nr_subgrids,
                           subgrid_size, nr_correlations);
  auto gbytes =
      1e-9 * bytes_gridder(nr_channels, total_nr_timesteps, nr_subgrids,
                           subgrid_size, nr_correlations);

  // Size of data structures
  const size_t sizeof_uvw =
      1ULL * nr_baselines * nr_timeslots * nr_timesteps * 3 * sizeof(float);
  const size_t sizeof_wavenumbers = 1ULL * nr_channels * sizeof(float);
  const size_t sizeof_visibilities =
      1ULL * nr_baselines * nr_timeslots * nr_timesteps * nr_channels *
      nr_correlations * sizeof(std::complex<float>);
  const size_t sizeof_spheroidal =
      1ULL * subgrid_size * subgrid_size * sizeof(float);
  const size_t sizeof_aterms = 1ULL * nr_stations * nr_timeslots *
                               nr_correlations * subgrid_size * subgrid_size *
                               sizeof(std::complex<float>);
  const size_t sizeof_subgrids = 1ULL * nr_subgrids * subgrid_size *
                                 subgrid_size * nr_correlations *
                                 sizeof(std::complex<float>);
  const size_t sizeof_metadata = 1ULL * nr_subgrids * sizeof(Metadata);

  // Initialize metadata
  Metadata metadata[nr_subgrids];
  for (auto bl = 0; bl < nr_baselines; bl++) {
    for (auto ts = 0; ts < nr_timeslots; ts++) {
      auto idx = bl * nr_timeslots + ts;

      // Metadata settings
      int baseline_offset = bl;
      int time_offset = ts * nr_timesteps;
      int aterm_index = 0;            // unused
      Baseline baseline = {0, 0};     // unused
      Coordinate coordinate = {0, 0}; // unused

      // Set metadata for current subgrid
      Metadata m = {baseline_offset, time_offset, nr_timesteps,
                    aterm_index,     baseline,    coordinate};
      metadata[bl * nr_timeslots + ts] = m;
    }
  }

  // Allocate device memory
  cu::DeviceMemory d_uvw(sizeof_uvw);
  cu::DeviceMemory d_wavenumbers(sizeof_wavenumbers);
  cu::DeviceMemory d_visibilities(sizeof_visibilities);
  cu::DeviceMemory d_spheroidal(sizeof_spheroidal);
  cu::DeviceMemory d_aterms(sizeof_aterms);
  cu::DeviceMemory d_metadata(sizeof_metadata);
  cu::DeviceMemory d_subgrids(sizeof_subgrids);

  // Copy metadata to device
  stream.memcpyHtoDAsync(d_metadata, metadata, sizeof_metadata);

  // Setup parameters
  std::vector<const void *> parameters = {&grid_size,
                                          &subgrid_size,
                                          &image_size,
                                          &w_step,
                                          &nr_channels,
                                          &nr_stations,
                                          d_uvw.parameter(),
                                          d_wavenumbers.parameter(),
                                          d_visibilities.parameter(),
                                          d_spheroidal.parameter(),
                                          d_aterms.parameter(),
                                          d_metadata.parameter(),
                                          d_subgrids.parameter()};

  // Kernel dimensions
  dim3 gridDim(nr_subgrids);
  dim3 blockDim(128);

  // Run kernel gridder
  run_kernel("idg_gridder", stream, function, gridDim, blockDim, parameters,
             gflops, gbytes, mvis);
}

void run_idg_degridder(cu::Device &device, cu::Stream &stream) {
  // Compile kernel
  std::vector<std::string> options = {
#if defined(__HIP__)
    "--offload-arch=" + device.getArch(),
    "-ffast-math",
#else
    "-arch=" + device.getArch(),
#endif
    "-I" + nvrtc::findIncludePath()
  };
  nvrtc::Program program(GetSourceDegridder(), "degridder");
  try {
    program.compile(options);
  } catch (nvrtc::Error &error) {
    std::cerr << program.getLog();
    throw;
  }
  cu::Module module(static_cast<const void *>(program.getPTX().data()));
  cu::Function function(module, "kernel_degridder");

  // Setup parameters
  const int grid_size = 8192;
  const unsigned nr_correlations = 4;
  const int subgrid_size = 32;
  const float image_size = 0.01;
  const float w_step = 0;
  const int nr_channels = 16;
  const int nr_stations = 30;
  const int nr_timeslots = 64;
  const int nr_timesteps = 128; // per subgrid

  // Derived parameters
  auto nr_baselines = (nr_stations * (nr_stations - 1)) / 2;
  auto nr_subgrids = nr_baselines * nr_timeslots;
  auto total_nr_timesteps = nr_subgrids * nr_timesteps;
  auto mvis = 1e-6 * total_nr_timesteps * nr_channels;

  // Amount of work performed
  auto gflops =
      1e-9 * flops_gridder(nr_channels, total_nr_timesteps, nr_subgrids,
                           subgrid_size, nr_correlations);
  auto gbytes =
      1e-9 * bytes_gridder(nr_channels, total_nr_timesteps, nr_subgrids,
                           subgrid_size, nr_correlations);

  // Size of data structures
  const size_t sizeof_uvw =
      1ULL * nr_baselines * nr_timeslots * nr_timesteps * 3 * sizeof(float);
  const size_t sizeof_wavenumbers = 1ULL * nr_channels * sizeof(float);
  const size_t sizeof_visibilities =
      1ULL * nr_baselines * nr_timeslots * nr_timesteps * nr_channels *
      nr_correlations * sizeof(std::complex<float>);
  const size_t sizeof_spheroidal =
      1ULL * subgrid_size * subgrid_size * sizeof(float);
  const size_t sizeof_aterms = 1ULL * nr_stations * nr_timeslots *
                               nr_correlations * subgrid_size * subgrid_size *
                               sizeof(std::complex<float>);
  const size_t sizeof_subgrids = 1ULL * nr_subgrids * subgrid_size *
                                 subgrid_size * nr_correlations *
                                 sizeof(std::complex<float>);
  const size_t sizeof_metadata = 1ULL * nr_subgrids * sizeof(Metadata);

  // Initialize metadata
  Metadata metadata[nr_subgrids];
  for (auto bl = 0; bl < nr_baselines; bl++) {
    for (auto ts = 0; ts < nr_timeslots; ts++) {
      auto idx = bl * nr_timeslots + ts;

      // Metadata settings
      int baseline_offset = bl;
      int time_offset = ts * nr_timesteps;
      int aterm_index = 0;            // unused
      Baseline baseline = {0, 0};     // unused
      Coordinate coordinate = {0, 0}; // unused

      // Set metadata for current subgrid
      Metadata m = {baseline_offset, time_offset, nr_timesteps,
                    aterm_index,     baseline,    coordinate};
      metadata[bl * nr_timeslots + ts] = m;
    }
  }

  // Allocate device memory
  cu::DeviceMemory d_uvw(sizeof_uvw);
  cu::DeviceMemory d_wavenumbers(sizeof_wavenumbers);
  cu::DeviceMemory d_visibilities(sizeof_visibilities);
  cu::DeviceMemory d_spheroidal(sizeof_spheroidal);
  cu::DeviceMemory d_aterms(sizeof_aterms);
  cu::DeviceMemory d_metadata(sizeof_metadata);
  cu::DeviceMemory d_subgrids(sizeof_subgrids);

  // Copy metadata to device
  stream.memcpyHtoDAsync(d_metadata, metadata, d_metadata.size());

  // Setup parameters
  std::vector<const void *> parameters = {&grid_size,
                                          &subgrid_size,
                                          &image_size,
                                          &w_step,
                                          &nr_channels,
                                          &nr_stations,
                                          d_uvw.parameter(),
                                          d_wavenumbers.parameter(),
                                          d_visibilities.parameter(),
                                          d_spheroidal.parameter(),
                                          d_aterms.parameter(),
                                          d_metadata.parameter(),
                                          d_subgrids.parameter()};

  // Kernel dimensions
  dim3 gridDim(nr_subgrids);
  dim3 blockDim(128);

  // Run kernel degridder
  run_kernel("idg_degridder", stream, function, gridDim, blockDim, parameters,
             gflops, gbytes, mvis);
}

int main() {
  // Read device number from envirionment
  char *cstr_deviceNumber = getenv("CUDA_DEVICE");
  unsigned deviceNumber = cstr_deviceNumber ? atoi(cstr_deviceNumber) : 0;

  // Setup CUDA
  cu::init();
  cu::Device device(deviceNumber);
  cu::Context context(CU_CTX_SCHED_BLOCKING_SYNC, device);
  cu::Stream stream;

  // Print CUDA device information
  std::cout << "Device " << deviceNumber << ": " << device.getName()
            << std::endl;
  std::cout << "Architecture: " << device.getArch() << std::endl;

  // Run benchmark
  run_peak_flops(device, stream);
  run_idg_gridder(device, stream);
  run_idg_degridder(device, stream);

  return EXIT_SUCCESS;
}
