#include <string>

std::string GetSourcePeakFlops();
std::string GetSourceGridder();
std::string GetSourceDegridder();