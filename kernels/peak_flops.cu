extern "C" {

__global__ void kernel_fma(float *ptr) {
  float x = threadIdx.x;
  float y = 0;

  for (int i = 0; i < 16384; i++) {
#pragma unroll
    for (int j = 0; j < 1024; j++) {
      x = x + x * y;
      y = y + y * x;
    }
  }

  ptr[blockIdx.x * blockDim.x + threadIdx.x] = x + y;
}

} // end extern "C"
