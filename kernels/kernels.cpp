#include "kernels.h"

extern const char _binary_kernels_peak_flops_cu_start,
    _binary_kernels_peak_flops_cu_end;

extern const char _binary_kernels_gridder_cu_start,
    _binary_kernels_gridder_cu_end;

extern const char _binary_kernels_degridder_cu_start,
    _binary_kernels_degridder_cu_end;

std::string GetSourcePeakFlops() {
  return std::string(&_binary_kernels_peak_flops_cu_start,
                     &_binary_kernels_peak_flops_cu_end);
}

std::string GetSourceGridder() {
  return std::string(&_binary_kernels_gridder_cu_start,
                     &_binary_kernels_gridder_cu_end);
}

std::string GetSourceDegridder() {
  return std::string(&_binary_kernels_degridder_cu_start,
                     &_binary_kernels_degridder_cu_end);
}